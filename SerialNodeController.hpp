#ifndef __SERIALNODECONTROLLER_HPP__
#define __SERIALNODECONTROLLER_HPP__

#include "definitions.h"
#include <qobject.h>
#include <string.h>		// To avoid memset warning
#include <stdint.h>		// For uint8_t
#include <unistd.h>
#include <signal.h>
#include <termios.h>
#include <fcntl.h>		// These are for serial comm

class QSocketNotifier;
class QString;

class SerialNodeController : public QObject
{
    Q_OBJECT

public:
    SerialNodeController(QObject *parent = 0);
    void startSocket(const char *path, unsigned int baud);
    void endSocket();
    enum Situation
    {
        openSuccess = 0,
        openFail,
        closed
    };

signals:
    void receivedNodePacket(Packet p);
    void hasSituationToLog(SerialNodeController::Situation s, QString m);

private:
    QSocketNotifier *socketReadNotifier;
    Packet packetIn;
    bool shouldReceive;
    bool packetShouldBreak7D;
    int openSerialSource(const char *device, const unsigned int baud);

    inline uint8_t readUInt8FromSerial(int fd)
    {
        uint8_t input;
        (void)read(fd, &input, 1);
        return input;
    }

    inline int closeSerialSource(const int descriptor)
    {
        return close(descriptor);
    }

private slots:
    void readNodeSocketByte(int nodeFd);
};

#endif
