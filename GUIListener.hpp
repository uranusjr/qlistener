#ifndef GUILISTENER_HPP
#define GUILISTENER_HPP

#include <qwidget.h>
class QLineEdit;
class QPushButton;
class QString;
class QTextEdit;

class GUIListener : public QWidget
{
	Q_OBJECT
	
public:
	GUIListener();
	QString devicePath();
	QString baud();
	void setListenState(bool isListening);

public slots:
	void appendLog(QString text);

signals:
	void listenButtonClicked();
	void stopButtonClicked();
	
private:
	QLineEdit *devicePathTextField;
	QLineEdit *baudTextField;
	QPushButton *listenButton;
	QPushButton *stopButton;
	QTextEdit *logTextField;
};

#endif
