#include "GUIListener.hpp"
#include <qlabel.h>
#include <qlayout.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include <qtextedit.h>

GUIListener::GUIListener() : QWidget()
{
	devicePathTextField = new QLineEdit("/dev/ttyUSB0", this);
	baudTextField = new QLineEdit("57600", this);
	logTextField = new QTextEdit(this);
	logTextField->setReadOnly(true);		// For log only
	
	listenButton = new QPushButton(tr("Listen"), this);
	stopButton = new QPushButton(tr("Stop"), this);
	// At startup, the listening is stopped; disable Stop button
	stopButton->setEnabled(false);
	
    QHBoxLayout *topBar = new QHBoxLayout();
	topBar->addWidget(new QLabel(tr("Device path:"), this));
	topBar->addWidget(devicePathTextField);
	topBar->addWidget(new QLabel(tr("Baud:"), this));
	topBar->addWidget(baudTextField);
	topBar->addWidget(listenButton);
	topBar->addWidget(stopButton);
	
        QVBoxLayout *whole = new QVBoxLayout();
	whole->addLayout(topBar);
	whole->addWidget(logTextField, 1);	// High stretching priority
        setLayout(whole);

	QObject::connect(listenButton, SIGNAL(clicked()), this, SIGNAL(listenButtonClicked()));
	QObject::connect(stopButton, SIGNAL(clicked()), this, SIGNAL(stopButtonClicked()));
}

void GUIListener::setListenState(bool isListening)
{
	devicePathTextField->setEnabled(!isListening);
	baudTextField->setEnabled(!isListening);
	listenButton->setEnabled(!isListening);
	stopButton->setEnabled(isListening);
}

QString GUIListener::devicePath()
{
	return devicePathTextField->text();
}

QString GUIListener::baud()
{
	return baudTextField->text();
}

void GUIListener::appendLog(QString text)
{
	logTextField->append(text);
}
