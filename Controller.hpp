#ifndef CONTROLLER_HPP
#define CONTROLLER_HPP

#include <qobject.h>
#include "definitions.h"
#include "SerialNodeController.hpp"
class GUIListener;
class QString;

class Controller : public QObject
{
	Q_OBJECT
	
public:
	Controller();
	void setWindow(GUIListener *w);
	
private:
	GUIListener *window;
	SerialNodeController *node;
	
private slots:
	void listen();
	void stopListening();
	void updatePacket(Packet p);
	void updateSituation(SerialNodeController::Situation s, QString message);
};

#endif
