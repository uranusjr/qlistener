#include "Controller.hpp"
#include "GUIListener.hpp"
#include "SerialNodeController.hpp"
#include <qstring.h>

Controller::Controller() : QObject()
{
	window = NULL;
	node = NULL;
}

void Controller::setWindow(GUIListener *w)
{
	QObject::disconnect(window);
	window = w;
	QObject::connect(w, SIGNAL(listenButtonClicked()), this, SLOT(listen()));
	QObject::connect(w, SIGNAL(stopButtonClicked()), this, SLOT(stopListening()));
}

void Controller::listen()
{
	window->setListenState(true);	// Is listening
	node = new SerialNodeController(this);
	QObject::connect(node, SIGNAL(receivedNodePacket(Packet)), this, SLOT(updatePacket(Packet)));
	QObject::connect
	(
		node, SIGNAL(hasSituationToLog(SerialNodeController::Situation, QString)),
		this, SLOT(updateSituation(SerialNodeController::Situation, QString))
	);
        node->startSocket(window->devicePath().toLocal8Bit().data(), window->baud().toInt());
}

void Controller::stopListening()
{
	node->endSocket();
	QObject::disconnect(node);
	delete node;
	window->setListenState(false);	// No longer listening
}

void Controller::updatePacket(Packet p)
{
	if (p.count() <= 4) return;		// Invalid packet (Precaution only)
	QString packetString;
	Packet::iterator i;
	for (i = p.begin(); i != p.end(); i++)
	{
		QString stringToAppend;
		stringToAppend.sprintf("%02X ", *i);
		packetString.append(stringToAppend);
	}
	window->appendLog(packetString);
}

void Controller::updateSituation(SerialNodeController::Situation s, QString message)
{
	window->appendLog(message);
	switch (s)
	{
		case SerialNodeController::openSuccess:
			window->setListenState(true);
			break;
		case SerialNodeController::openFail:
		case SerialNodeController::closed:
			window->setListenState(false);
			break;
	}
}
