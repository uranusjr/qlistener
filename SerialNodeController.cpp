#include "SerialNodeController.hpp"
#include <qsocketnotifier.h>
#include <qtimer.h>
#include <qstring.h>

SerialNodeController::SerialNodeController(QObject *parent) : QObject(parent)
{
    packetShouldBreak7D = false;
    shouldReceive = true;
}

void SerialNodeController::startSocket(const char *path, unsigned int baud)
{
    int fd = openSerialSource(path, baud);
    switch (fd)
    {
        case -2:
            emit hasSituationToLog(openFail, "Unsupported baud");
            return;
        case -1:
            emit hasSituationToLog(openFail, "Serial open failed");
            return;
        default:
            QString message;
            message.sprintf("Opening device %s at %d with %d baud...", path, fd, baud);
            emit hasSituationToLog(openSuccess, message);
    }
    socketReadNotifier = new QSocketNotifier(fd, QSocketNotifier::Read);
    connect(socketReadNotifier, SIGNAL(activated(int)), this, SLOT(readNodeSocketByte(int)));
}

void SerialNodeController::endSocket()
{
    disconnect(socketReadNotifier, SIGNAL(activated(int)), 0, 0);
    closeSerialSource(socketReadNotifier->socket());
    emit hasSituationToLog(closed, "Serial closed");
    delete socketReadNotifier;
}

void SerialNodeController::readNodeSocketByte(int fd)
{
    packetIn.append(readUInt8FromSerial(fd));
    emit receivedNodePacket(packetIn);
    if (packetIn.last() == 0x7E)
    {
        if (packetIn.count() > 12)
        {
            emit receivedNodePacket(packetIn);
        }
        packetShouldBreak7D = false;
        shouldReceive = true;
        packetIn.clear();
        packetIn.append(0x7E);
    }
    else if (shouldReceive == false) return;
    else if (packetIn.back() == 0x7D)
    {
        packetIn.pop_back();
        packetShouldBreak7D = true;
    }
    else if (packetShouldBreak7D)
    {
        if (packetIn.last() == 0x5D) packetIn.last() = 0x7D;
        else if (packetIn.last() == 0x5E) packetIn.last() = 0x7E;
        else shouldReceive = false;		// Error, should wait until 0x7E
        packetShouldBreak7D = false;
    }
}

int SerialNodeController::openSerialSource(const char *device, const unsigned int baud)
{
    struct termios options;
    int fd = -1;
    fd = open(device, O_RDWR | O_NOCTTY | O_NONBLOCK);
    if (fd < 0) return fd;

    memset(&options, 0, sizeof(options));
    options.c_iflag = IGNPAR | IGNBRK;
    switch (baud)
    {
#ifdef B115200
        case 115200:
            options.c_cflag = B115200 | CREAD | CS8 | CLOCAL;
            cfsetspeed(&options, B115200);
            break;
#endif
#ifdef B38400
        case 38400:
            options.c_cflag = B38400 | CREAD | CS8 | CLOCAL;
            cfsetspeed(&options, B38400);
            break;
#endif
#ifdef B19200
        case 19200:
            options.c_cflag = B19200 | CREAD | CS8 | CLOCAL;
            cfsetspeed(&options, B19200);
            break;
#endif
#ifdef B9600
        case 9600:
            options.c_cflag = B9600 | CREAD | CS8 | CLOCAL;
            cfsetspeed(&options, B9600);
            break;
#endif
#ifdef B57600
        case 57600:
            options.c_cflag = B57600 | CREAD | CS8 | CLOCAL;
            cfsetspeed(&options, B57600);
            break;
#endif
#ifdef B262144
        case 262144:
            options.c_cflag = CREAD | CS8 | CLOCAL;
            cfsetspeed(&options, B262144);
#endif
        default:
            closeSerialSource(fd);
            return -2;
    }
    options.c_oflag = 0;
    options.c_cc[VMIN] = 0;
    options.c_cc[VTIME] = 10;
    tcflush(fd, TCIFLUSH);
    tcsetattr(fd, TCSAFLUSH, &options);	// Apply the options to fd

    return fd;
}
