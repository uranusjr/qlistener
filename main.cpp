#include "GUIListener.hpp"
#include "Controller.hpp"
#include <qapplication.h>
#include <qsizepolicy.h>

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);
	GUIListener *window = new GUIListener;
	Controller *controller = new Controller;
	controller->setWindow(window);
	
        //app.setMainWidget(window);
	window->resize(550, 600);
	window->show();
	
	return app.exec();
}
