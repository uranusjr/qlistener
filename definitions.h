#ifndef DEFINITIONS_H
#define DEFINITIONS_H

#include <qlist.h>
#include <stdint.h>		// For uint8_t

typedef QList<uint8_t> Packet;

#endif
